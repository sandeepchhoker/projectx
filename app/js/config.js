(function() {
    'use strict';

	angular.module('myApp')
	.config(config);

	config.$inject = ['$routeProvider'];
	
	function config($routeProvider){
		$routeProvider.
		when('/login',{
			templateUrl: 'views/login.html',
			controller: 'AuthenticationController',
			controllerAs: 'vm'
		}).
		when('/register',{
			templateUrl: 'views/register.html',
			controller: 'AuthenticationController',
			controllerAs: 'vm'
		}).
		when('/',{
			templateUrl: 'views/meetings.html',
			controller: 'MeetingController',
			controllerAs: 'vm'
		}).
		otherwise({
			redirectTo:'/'
		});
	}
})();