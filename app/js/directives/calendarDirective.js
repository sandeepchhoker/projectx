(function() {
    'use strict';

    angular.module('myApp')
    .directive("calendar", calendar);

    function calendar() {
        return {
            restrict: "E",
            templateUrl: "../../views/templates/calendar.html",
            scope: {
                openPopUp: '&',
                openDay: '&'
            },
            controller: CalendarController,
            controllerAs: 'vm',
            bindToController: true
        };
    }

    CalendarController.$inject = ['$scope','manageMeetingService'];

    function CalendarController($scope, manageMeetingService) {
        var vm = this,
            start,
            meetings = [],
            calendarMeetings = [];
        vm.selected= removeTime( moment() );
        vm.month= vm.selected.clone();
        vm.select = select;
        vm.next = next;
        vm.previous = previous;
        vm.showMeeting = showMeeting;
        vm.showDay = showDay;
        vm.manageMeetingService = manageMeetingService;

        manageMeetingService.setSelectedDate(vm.selected);

        $scope.$watch('vm.manageMeetingService.refreshMonthMeetings', function(){
            if(manageMeetingService.refreshMonthMeetings){
                activate();
                manageMeetingService.refreshMonthMeetings = false;
            }
        });

        activate();

        function activate(){
            meetings= manageMeetingService.getAllMeetings();
            meetings.$loaded()
                .then(function(data) {
                    createMonthCalendar();
                })
                .catch(function(error) {
                    alert("Error:", error);
                });

        }

        function createMonthCalendar(){
            calendarMeetings=[];
            for(var i in meetings){
                if(typeof meetings[i]==="object"){
                   calendarMeetings.push(meetings[i]);
                }
            }
            manageMeetingService.setSelectedDate(vm.selected);
            start = vm.selected.clone();
            start.date(1);
            removeDays(start.day(0));
            buildMonth(vm, start, vm.month);
        }
        function showMeeting(meeting){
            vm.openPopUp({meeting:meeting});
        }
        function select(day) {
            vm.selected = day.date;
            manageMeetingService.setSelectedDate(vm.selected);
        }
        function showDay(day) {
            select(day);
            vm.openDay();
        }
        function next() {
            var next = vm.month.clone();
            removeDays(next.month(next.month() + 1)).date(1);
            vm.month.month(vm.month.month() + 1);
            buildMonth(vm, next, vm.month);
        }
        function previous() {
            var previous = vm.month.clone();
            removeDays(previous.month(previous.month() - 1).date(1));
            vm.month.month(vm.month.month() - 1);
            buildMonth(vm, previous, vm.month);
        }
        
        function removeDays(date) {
            return date.day(0).hour(0).minute(0).second(0).millisecond(0);
        }
        function removeTime(date) {
            return date.hour(0).minute(0).second(0).millisecond(0);
        }
        function buildMonth(vm, start, month) {
            vm.weeks = [];
            var done = false,
                date = start.clone(),
                monthIndex = date.month(),
                count = 0;
            while (!done) {
                vm.weeks.push({
                    days: buildWeek(date.clone(), month)
                });
                date.add(1, "w");
                done = count++ > 2 && monthIndex !== date.month();
                monthIndex = date.month();
            }
        }
        function buildWeek(date, month) {
            var days = [];
            for (var i = 0; i < 7; i++) {
                days.push({
                    name: date.format("dd").substring(0, 1),
                    number: date.date(),
                    isCurrentMonth: date.month() === month.month(),
                    isToday: date.isSame(new Date(), "day"),
                    date: date,
                    meetings: getMeetings(date)
                });
                date = date.clone();
                date.add(1, "d");
            }
            return days;
        }
        function getMeetings(date){
            var meetings = [];
            for(var i in calendarMeetings){
                var duedate = calendarMeetings[i].duedate.split(" ")
                    .splice(0,3)
                    .join(" ");
                if(duedate == date.format("MMM Do YY")){
                    meetings.push(calendarMeetings[i]);
                }
            }
            return meetings;
        }
    }
})();