(function() {
    'use strict';

    angular.module('myApp')
    .directive('timeSlot', timeSlot);

    function timeSlot() {
        return {
            templateUrl: '../../views/templates/timeSlot.html',
            restrict: "E",
        }
    }
})();