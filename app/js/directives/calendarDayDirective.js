(function() {
    'use strict';

    angular.module('myApp')
    .directive('calendarDay', calendarDay);

    function calendarDay() {
        return {
            templateUrl: '../../views/templates/calendarDay.html',
            restrict: "E",
            scope: {
                openPopUp: '&openPopUp'
            },
            controller: CalendarDayController,
            controllerAs: 'vm',
            bindToController: true
        }
    }

    CalendarDayController.$inject = ['$scope','manageMeetingService'];

    function CalendarDayController($scope, manageMeetingService) {
        var vm = this;

        var meetings = [],
            calendarMeetings=[];

        vm.editSlot = false;
        vm.index = -1;
        vm.selectedTime = {};
        vm.meeting={};
        vm.getDate = getDate;
        vm.addMeeting = addMeeting;
        vm.showMeeting = showMeeting;
        vm.manageMeetingService = manageMeetingService;

        $scope.$watch('vm.manageMeetingService.refreshDayMeetings', function(){
            if(manageMeetingService.refreshDayMeetings){
                activate();
                manageMeetingService.refreshDayMeetings = false;
            }
        });

        activate();
        function activate() {
            meetings = manageMeetingService.getAllMeetings();
            meetings.$loaded()
                .then(function(data) {
                    //console.log(data);
                    setDayMeetings();
                })
                .catch(function(error) {
                    console.error("Error:", error);
                });

        }

        function showMeeting(meeting){
            vm.openPopUp({meeting:meeting});
        }

        function addMeeting(dayTime) {
            var meeting={};
            meeting.duedate= getDate()+' '+dayTime.time;
            vm.openPopUp({meeting:meeting});
        }

        function getDate() {
            return manageMeetingService.getSelectedDate().format("MMM Do YY");
        }

        function setDayMeetings(){
            calendarMeetings=[];
            for(var i in meetings){
                if(typeof meetings[i]==="object"){
                   calendarMeetings.push(meetings[i]);
                }
            }
            vm.dayTimes = [];
            var time = 12;
            var date = vm.getDate();
            for(var i=0; i<12 ; i++){
                vm.dayTimes.push({
                    time: time+' am',
                    meetings: getMeetings(date+' '+time+' am')
                });
                time = time%12 + 1;
            }
            for(var i=0; i<12 ; i++){
                vm.dayTimes.push({
                    time: time+' pm',
                    meetings: getMeetings(date+' '+time+' pm')
                });
                time = time%12 + 1;
            }
        }

        function getMeetings(date){
            var meetings = [];
            for(var i in calendarMeetings){
                if(calendarMeetings[i].duedate === date){
                    meetings.push(calendarMeetings[i]);
                }
            }
            return meetings;
        }
    }
})();