(function() {
    'use strict';

    angular.module('myApp')
    .controller('AuthenticationController',AuthenticationController)

    AuthenticationController.$inject=['$location','$cookieStore','authenticationService','manageMeetingService'];

    function AuthenticationController($location, $cookieStore, authenticationService, manageMeetingService){
        var vm = this;
        vm.user={};
    	vm.login=login;
        vm.register=register;
        vm.redirectToRegister=function(){ $location.path('/register'); };
        
        function login(){
    	    authenticationService.login(vm.user)
                .then(function(userData){
                    $location.path('/');
                    $cookieStore.put('currentUser',userData);
                    manageMeetingService.setFirebaseObject(userData);
                },function(error){
                        vm.errorMessage=error.toString();
                    }
                );
        }

        function register(){
                authenticationService.register(vm.user)
                .then(function(user){
                    $location.path('/login');
                }).catch(function(error){ 
                    vm.errormessage=error.toString();
                });
        }
        
    }
})();