(function() {
    'use strict';

	angular.module('myApp')
	.controller('NavigationController',NavigationController);
	
	NavigationController.$inject=['$location','$cookieStore','authenticationService','manageMeetingService'];

	function NavigationController($location, $cookieStore, authenticationService, manageMeetingService){
        var vm = this;
	    vm.logout=logout;
		vm.isActive = function(viewLocation){ return viewLocation === $location.path(); };
		
	    activate();
	    
	    function activate(){

	    }
		function logout(){
		 	authenticationService.logout();
		 	$cookieStore.remove('currentUser');
		 	manageMeetingService.removeCurrentUser();
		 	$location.path('/');
		}
	}
})();
