(function() {
    'use strict';

    angular.module('myApp')
        .controller('MeetingController', MeetingController);

    MeetingController.$inject = ['manageMeetingService'];

    function MeetingController(manageMeetingService) {
        var vm = this;
        vm.meetings = [];
        vm.showMonth = true;
        vm.addMeeting = addMeeting;
        vm.deleteMeeting = deleteMeeting;
        vm.onClickDay = onClickDay;
        vm.onClickMonth = onClickMonth;
        vm.displayMeetingPopup = false;
        vm.displayMeeting= {};
        vm.openPopUp = openPopUp;
        vm.closePopUp = closePopUp;

        activate();

        function activate() {
            vm.meetings = manageMeetingService.getAllMeetings();
        }
        function onClickMonth(){
            vm.showMonth=true;
        }

        function onClickDay(){
            vm.showMonth=false;
            manageMeetingService.refreshMeetings();
        }

        function openPopUp(meeting){
            console.log("pop up opened");
            if(meeting){
                vm.displayMeeting=meeting;
                vm.displayMeetingPopup=true;
            }
        }
        function closePopUp(){
            vm.displayMeetingPopup=false;
        }
        function addMeeting() {
            manageMeetingService.addMeeting(vm.displayMeeting).then(function(ref) {
                    vm.displayMeeting = {};
                    manageMeetingService.refreshMeetings();
                },
                function(error) {
                    alert(error);
                }
            );
        }
        function deleteMeeting() {
            manageMeetingService.deleteMeeting(vm.displayMeeting).then(function(ref) {
                     manageMeetingService.refreshMeetings();
                 });
        }

    }
})();