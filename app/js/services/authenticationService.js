(function() {
    'use strict';

	angular.module('myApp').
	factory('authenticationService',authenticationService);

	authenticationService.$inject=['$firebase','$firebaseObject','$firebaseAuth'];

	function authenticationService($firebase, $firebaseObject, $firebaseAuth){
		var ref = new Firebase('https://airhawks.firebaseio.com/');
	    var userAuth = $firebaseAuth(ref);
	    
	    return {
	        login: login,
	        logout: logout,	
			register: register
		};

		function login(user){
	      	return userAuth.$authWithPassword({
	     		email:user.email,
	 			password : user.password
			});
	    }
		function logout(user){
			return userAuth.$unauth();
		}
		function register(user) {
	     	return userAuth.$createUser({
			        email: user.email,
			        password: user.password
			      })
		         .then(function(userData){
		         	setInitialDataModel(userData,user);
		         });
		}
		function setInitialDataModel(userData, user){
			var ref = new Firebase('https://airhawks.firebaseio.com/users/'+userData.uid);
	        var firebaseUsers = $firebaseObject(ref);

	        var userInfo = {
	          date : Firebase.ServerValue.TIMESTAMP,
	          regUser : userData.uid,
	          firstname: user.firstname,
	          lastname : user.lastname,
	          email: user.email
	        };

	        firebaseUsers.userData=userInfo;
	        firebaseUsers.totalMeetings=0;
	        firebaseUsers.meetings=[];

	        firebaseUsers.$save();
		}
	}
})();