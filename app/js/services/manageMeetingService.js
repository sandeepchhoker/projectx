(function() {
    'use strict';

    angular.module('myApp').
    factory('manageMeetingService', manageMeetingService)

    manageMeetingService.$inject = ['$rootScope','$firebase','$firebaseArray','$filter','$cookieStore'];

    function manageMeetingService($rootScope, $firebase, $firebaseArray, $filter, $cookieStore) {
        var ref,
            array=[],
            selectedDate;


        activate();

        return {
            refreshDayMeetings: false,
            refreshMonthMeetings: false,
            refreshMeetings: refreshMeetings,
            addMeeting: addMeeting,
            deleteMeeting: deleteMeeting,
            getAllMeetings: getAllMeetings,
            getCurrentUser: getCurrentUser,
            removeCurrentUser: removeCurrentUser,
            setFirebaseObject: setFirebaseObject,
            setSelectedDate: setSelectedDate,
            getSelectedDate: getSelectedDate
        };


        function activate(){
            var data = $cookieStore.get('currentUser');
            if(data){
                setFirebaseObject(data);
            }
            selectedDate = moment();
        }

        function getCurrentUser(){
            return user;
        }

        function removeCurrentUser(){
            $rootScope.currentUser = false;
            array=[];
            ref=[];
        }

        function setFirebaseObject(user) {
            $rootScope.currentUser = user;
            ref = new Firebase('https://airhawks.firebaseio.com/users/' + user.uid + "/meetings");
            array = $firebaseArray(ref);
        }

        function getAllMeetings(){
            return array;
        }

        function addMeeting(newMeeting) {
            var data = {};
            data.name = newMeeting.name;
            data.date = Firebase.ServerValue.TIMESTAMP;
            data.description = newMeeting.description;
            data.duedate = newMeeting.duedate;
            return array.$add(data);
        }

        function deleteMeeting(meeting) {
            return array.$remove(meeting);
        }
        function setSelectedDate(selected){
            selectedDate=selected;
        }
        function getSelectedDate(){
            return selectedDate;
        }
        function refreshMeetings(){
            this.refreshDayMeetings=true;
            this.refreshMonthMeetings=true;
        }
    }
})();